FROM node:16.14
WORKDIR /front/

COPY package.json package-lock.json ./
RUN npm install 
RUN npm install react-scripts@3.4.1 -g 
RUN npm install axios
RUN npm install react-router-dom

COPY . ./
EXPOSE 3000