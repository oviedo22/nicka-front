import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { useNavigate } from "react-router-dom";
import { Navigation } from '../Navigation/Navigation';


export const BookingForm = () => {
    const navigate = useNavigate();
    const [viaje, setViaje] = useState(
        ''
    );

    const [ciudades, setCiudades] = useState('')
    const [flight_class, setFlightClass] = useState('')

    const [errors, setErrors] = useState('')

    const handleChange = (event) => {
        setViaje({ ...viaje, [event.target.name]: event.target.value })

    }

    const handleSubmit = (e) => {
        e.preventDefault()
        console.log(viaje)
        axios.post('http://127.0.0.1:8085/bookings/', viaje, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(function (response) {
                console.log(response)
                navigate(`info/${1}`);
            })
            .catch(function (error) {
                setErrors("El formulario contiene errores que debe revisar. Por favor intente nuevamente")
            })
    }

    useEffect(() => {
        axios.get(
            'http://127.0.0.1:8085/city/'
        )
            .then((res) => {
                setCiudades(res.data);
            })
            .catch((error) => console.log(error));

        axios.get(
            'http://127.0.0.1:8085/flight-class/'
        )
            .then((res) => {
                setFlightClass(res.data);
            })
            .catch((error) => console.log(error));

    }, []);

    return (

        <React.Fragment>

            <Navigation> </Navigation>


            <div className="container">
                <form className='white mb-3' onSubmit={handleSubmit}>
                    <h3 className="mb-3">Nuevo Viaje</h3>
                    <div className="input-field mb-3">
                        <label htmlFor="flight_type" className="form-label">Tipo de Vuelo</label>
                        <select name="flight_type" value={viaje.flight_type} defaultValue="" onChange={handleChange} className="form-control">
                            <option value="" disabled>Seleccione una opcion</option>
                            <option value="Ida" key="1" required>Ida</option>
                            <option value="Ida/Vuelta" key="2" required>Ida/Vuelta</option>
                        </select>

                    </div>
                    <div className="input-field mb-3">
                        <label htmlFor="departure_city" className="form-label">Ciudad de Partida</label>
                        <select name="departure_city" defaultValue="" onChange={handleChange} className="form-control">
                            <option value="" disabled>Seleccione una opcion</option>

                            {
                                ciudades && ciudades.map(city => (
                                    <option key={city.id} value={city.id} required>{city.name}</option>
                                ))
                            }
                        </select>
                    </div>

                    <div className="input-field mb-3">
                        <label htmlFor="arriving_city" className="form-label">Ciudad de Llegada</label>
                        <select name="arriving_city" defaultValue="" onChange={handleChange} className="form-control">
                            <option value="" disabled>Seleccione una opcion</option>

                            {
                                ciudades && ciudades.map(city => (
                                    <option key={city.id} value={city.id} required>{city.name}</option>
                                ))
                            }
                        </select>
                    </div>
                    <div className="input-field mb-3">
                        <label htmlFor="departure_date" className="form-label">Fecha de partida</label>
                        <input type="date" name="departure_date" className="form-control" defaultValue={viaje.departure_date} onChange={handleChange} required />
                    </div>
                    <div className="input-field mb-3">
                        <label htmlFor="departure_time" className="form-label">Hora de partida</label>
                        <input type="time" name="departure_time" className="form-control" defaultValue={viaje.departure_time} onChange={handleChange} required />
                    </div>
                    <div className="input-field mb-3">
                        <label htmlFor="passengers_count" className="form-label">Cantidad de Pasajeros</label>
                        <input type="number" name="passengers_count" className="form-control" defaultValue={viaje.passengers_count} onChange={handleChange} required />
                    </div>
                    <div className="input-field mb-3">
                        <label htmlFor="flight_class" className="form-label">Clase de Vuelo</label>
                        <select name="flight_class" defaultValue="" onChange={handleChange} className="form-control">
                            <option value="" disabled>Seleccione una opcion</option>

                            {
                                flight_class && flight_class.map && flight_class.map(fclass => (
                                    <option key={fclass.id} value={fclass.id} required>{fclass.name}</option>
                                ))
                            }
                        </select>
                    </div>
                    <div className="input-field">
                        <button className="btn btn-primary" type="submit">Enviar</button>
                    </div>
                    
                    {errors!=='' && <div className="alert alert-danger mt-3 mb-3" role="alert">
                    {
                        errors
                    }
                    </div>}

                        

                </form>
            </div>
        </React.Fragment>
    );

}
