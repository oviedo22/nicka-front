import React, { useEffect, useState } from 'react'
import { BookingCard } from '../Card/BookingCard'
import { Navigation } from '../Navigation/Navigation';
import axios from 'axios'


export const Home = () => {

  const [viaje, setViaje] = useState('')
  const [voidObject, setVoidObject] = useState('')
  // GET
  useEffect(() => {


    axios
      .get(
        'http://127.0.0.1:8085/bookings/'
      )
      .then((res) => {
        console.log("GET")
        setViaje(res.data);
        setVoidObject(false)

      })
      .catch((error) => {
        console.log(error)
        setVoidObject(true)

      });



    // const voidInfo = () => {
    //   if (Object.keys(viaje)) {
    //     console.log("Void")

    //     setVoidObject(true)
    //   }
    // }



  }, []);

  return (
    <React.Fragment>

      <Navigation> </Navigation>

      <div className='mt-3 mb-3 container-fluid'>

        {voidObject ? (
          <div className="row justify-content-center">
            <div className="col-md-12 col-sm-12">
              <div className="card shadow-lg border-0 rounded-lg mt-5 mx-auto" style={{ width: '30rem' }}>
                <h3 className="card-header display-1 text-muted text-center">
                  Ups, no hemos encontrado elementos aun
                </h3>

                <span className="card-subtitle mt-2 mb-2 text-muted text-center">
                  Si deseas crear un nuevo viaje, presiona en el boton inferior:
                </span>

                <div className="card-body mx-auto">
                  <a href="new-booking"
                    className="btn btn-info text-white"> Nuevo Viaje </a>
                </div>
              </div>
            </div>
          </div>

        ) : (
          <div className= "row justify-content-evenly">

            <h2 className="mt-3 mb-3 text-center">Viajes Encontrados</h2>

            {
              viaje && viaje.map && viaje.map(booking => (
                <BookingCard key={booking.id} booking={booking}></BookingCard>
              ))
            }

          </div>

        )}


        {/* {
          viaje && viaje.map && viaje.map(booking => (
            <BookingCard key={booking.id} booking={booking}></BookingCard>
          ))
        } */}
      </div>
    </React.Fragment>
  )
}
