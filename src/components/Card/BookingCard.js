import React, { useState, useEffect } from 'react'
import axios from 'axios'

export const BookingCard = ({ booking }) => {
  const [departure_city, setCityDeparture] = useState('');
  const [arriving_city, setCityArriving] = useState('');
  const [flight_class, setFlightClass] = useState('');


  useEffect(() => {
    axios.get(`http://127.0.0.1:8085/city/${booking.departure_city}`
    )
      .then((res) => {
        setCityDeparture(res.data);
      })
      .catch((error) => console.log(error));

    axios.get(`http://127.0.0.1:8085/city/${booking.arriving_city}`
    )
      .then((res) => {
        setCityArriving(res.data);
      })
      .catch((error) => console.log(error));

    axios.get(
      `http://127.0.0.1:8085/flight-class/${booking.flight_class}`
    )
      .then((res) => {
        setFlightClass(res.data);       
      })
      .catch((error) => console.log(error));
  }, []);

  return (
    <div key={booking.id} className="card col-md-4">
      <div className="card-body text-center">
        <h5 className="card-title">Numero Vuelo: {booking.id}</h5>
        <p className="card-text">Tipo Viaje: {booking.flight_type}</p>
        <p className="card-text">Ciudad Salida: {departure_city.name}</p>
        <p className="card-text">Ciudad Llegada: {arriving_city.name}</p>
        <p className="card-text">Fecha y Hora: {booking.departure_date} - {booking.departure_time}</p>
        <p className="card-text">Cantidad de Pasajeros: {booking.passengers_count}</p>
        <p className="card-text">Clase de Vuelo: {flight_class.name}</p>
      </div>
    </div>

  )
}


