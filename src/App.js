import React from 'react'

import { AppRouter } from './routers/AppRouter';
import './styles/App.css';

export const App = () => {
  
  return <AppRouter />
}
