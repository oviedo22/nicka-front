import React from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'

import { Home } from '../components/Home/Home'
import { BookingForm } from '../components/form/BookingForm'
import { Info } from '../components/Info/Info'
import { ErrorPage } from '../components/ui/ErrorPage'


export const AppRouter = () => {
    return (
      <Router>
        <div>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="new-booking" element={<BookingForm />} />
            <Route exact path="new-booking/info/:data" element={<Info />} />  
            <Route path="*" element={ <ErrorPage /> }/>
          </Routes>
        </div>
      </Router>
    )
  }
  
